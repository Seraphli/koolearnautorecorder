﻿namespace KoolearnAutoRecorder
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.webBrowser = new System.Windows.Forms.WebBrowser();
            this.btnJump = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.textBoxURL = new System.Windows.Forms.TextBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.textBoxCurURL = new System.Windows.Forms.TextBox();
            this.btnClean = new System.Windows.Forms.Button();
            this.lMonitor = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.webBrowser);
            this.panel1.Location = new System.Drawing.Point(12, 33);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1240, 655);
            this.panel1.TabIndex = 0;
            // 
            // webBrowser
            // 
            this.webBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser.Location = new System.Drawing.Point(0, 0);
            this.webBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser.Name = "webBrowser";
            this.webBrowser.Size = new System.Drawing.Size(1240, 655);
            this.webBrowser.TabIndex = 0;
            this.webBrowser.NewWindow += new System.ComponentModel.CancelEventHandler(this.webBrowser_NewWindow);
            // 
            // btnJump
            // 
            this.btnJump.Location = new System.Drawing.Point(756, 696);
            this.btnJump.Name = "btnJump";
            this.btnJump.Size = new System.Drawing.Size(75, 23);
            this.btnJump.TabIndex = 1;
            this.btnJump.Text = "跳转";
            this.btnJump.UseVisualStyleBackColor = true;
            this.btnJump.Click += new System.EventHandler(this.btnJump_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(837, 696);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(75, 23);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "开始监控";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // textBoxURL
            // 
            this.textBoxURL.Location = new System.Drawing.Point(12, 696);
            this.textBoxURL.Name = "textBoxURL";
            this.textBoxURL.Size = new System.Drawing.Size(738, 21);
            this.textBoxURL.TabIndex = 3;
            this.textBoxURL.Text = "http://www.koolearn.com/member?_method=myCourse";
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(918, 696);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 23);
            this.btnStop.TabIndex = 4;
            this.btnStop.Text = "停止监控";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // textBoxCurURL
            // 
            this.textBoxCurURL.Location = new System.Drawing.Point(12, 6);
            this.textBoxCurURL.Name = "textBoxCurURL";
            this.textBoxCurURL.Size = new System.Drawing.Size(981, 21);
            this.textBoxCurURL.TabIndex = 5;
            // 
            // btnClean
            // 
            this.btnClean.Location = new System.Drawing.Point(999, 696);
            this.btnClean.Name = "btnClean";
            this.btnClean.Size = new System.Drawing.Size(75, 23);
            this.btnClean.TabIndex = 6;
            this.btnClean.Text = "清理缓存";
            this.btnClean.UseVisualStyleBackColor = true;
            this.btnClean.Click += new System.EventHandler(this.btnClean_Click);
            // 
            // lMonitor
            // 
            this.lMonitor.AutoSize = true;
            this.lMonitor.Location = new System.Drawing.Point(1102, 701);
            this.lMonitor.Name = "lMonitor";
            this.lMonitor.Size = new System.Drawing.Size(35, 12);
            this.lMonitor.TabIndex = 7;
            this.lMonitor.Text = "False";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 729);
            this.Controls.Add(this.lMonitor);
            this.Controls.Add(this.btnClean);
            this.Controls.Add(this.textBoxCurURL);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.textBoxURL);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnJump);
            this.Controls.Add(this.panel1);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "新东方在线自动录制器";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.WebBrowser webBrowser;
        private System.Windows.Forms.Button btnJump;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TextBox textBoxURL;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.TextBox textBoxCurURL;
        private System.Windows.Forms.Button btnClean;
        private System.Windows.Forms.Label lMonitor;
    }
}

