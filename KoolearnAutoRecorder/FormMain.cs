﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WindowsInput;

namespace KoolearnAutoRecorder
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }

        private void btnJump_Click(object sender, EventArgs e)
        {
            webBrowser.Navigate(textBoxURL.Text);
        }

        string OldURL = "";
        bool IsMonitor = false;
        private void Monitor()
        {
            while (IsMonitor)
            {
                var NewURL = webBrowser.Url.ToString();
                if (OldURL != NewURL)
                {
                    OldURL = NewURL;
                    PressF10();
                    Action tmp = new Action(PressF10AfterSecond);
                    tmp.BeginInvoke(tmp.EndInvoke, null);
                }
                textBoxCurURL.Text = NewURL;
                System.Threading.Thread.Sleep(100);
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            IsMonitor = true;
            lMonitor.Text = IsMonitor.ToString();
            OldURL = webBrowser.Url.ToString();
            Action tmp = new Action(Monitor);
            tmp.BeginInvoke(tmp.EndInvoke, null);
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            IsMonitor = false;
            lMonitor.Text = IsMonitor.ToString();
        }

        private void PressF10()
        {
            InputSimulator.SimulateKeyPress(VirtualKeyCode.F10);
        }

        private void PressF10AfterSecond()
        {
            System.Threading.Thread.Sleep(5000);
            InputSimulator.SimulateKeyPress(VirtualKeyCode.F10);
        }

        private void webBrowser_NewWindow(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            webBrowser.Navigate(webBrowser.StatusText);
        }

        private void btnClean_Click(object sender, EventArgs e)
        {
            WebBrowserHelper.ClearCache();
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsMonitor)
            {
                IsMonitor = false;
                System.Threading.Thread.Sleep(250);
            }
        }
    }
}
